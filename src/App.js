import React, { useState, useRef, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';


const ProductRow = ({ product }) => {
 
  return (
    <tr>
      <td>
       <a href={product.url}>{product.title}</a></td>
      <td>{product.price}</td>
    </tr>
  );
}

const  ProductTable = ({ products, filterText }) => {
  const rows = [];


  products.forEach((product) => {
    rows.push(
      <ProductRow
        product={product}
        key={product.title} />
    );
  });

  return (
    <table>
      <thead>
        <tr>
          <th>Product Name</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </table>
  );
}

const  SearchBar = ({
  filterText,
  onFilterTextChange,
  maxValue,
  sliderValue,
  onSliderValueChange,
  subscriptionValue,
  onSubscriptionValueChange,
}) => {
 
  return (<form>

      <label>
      Search for relevant product
      <input 
        type="text" 
        value={filterText} placeholder="Enter search term" 
        onChange={(e) => onFilterTextChange(e.target.value)} />
</label>
      <label>
      	Show items priced up to {sliderValue}
  		<input type="range" min="0" max={maxValue} value={sliderValue}  onChange={(e) => onSliderValueChange(e.target.value)}  className="slider" id="myRange"/>
    	</label>

      <label>
      	Only include subscriptions?
      	<div className="flipswitch">
	    <input type="checkbox" name="flipswitch" className="flipswitch-cb" id="fs" checked={subscriptionValue}  onChange={onSubscriptionValueChange} />
	    <label className="flipswitch-label" onClick={onSubscriptionValueChange}>
	        <div className="flipswitch-inner"></div>
	        <div className="flipswitch-switch"></div>
	    </label>
	</div>
    	</label>
    </form>)
}


export default function App() {

  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [itemsOnPage, setItemsOnPage] = useState(8);
  const [pageNumber, setPageNumber] = useState(1);
  const [allItemsFound, setAllItemsFound] = useState(0);

  const [filterText, setFilterText] = useState('');
  const maxValue= 150;
  const [sliderValue, setSliderValue] = useState(maxValue);
  const [subscriptionValue, setSubscriptionValue] = useState(false);


const changeSubscriptionValue = () =>{
		setSubscriptionValue(!subscriptionValue)
}

const goLast = () =>{
	setPageNumber(1 + parseInt (allItemsFound / itemsOnPage) )
}





 useEffect(() => {
let includeSubscriptionValue = (subscriptionValue?"&subscription=true":"" )
 let URL =(`http://localhost:3000/data/products.json/products?&q=${filterText}&_page=${pageNumber}&_limit=${itemsOnPage}&price_lte=${sliderValue}${includeSubscriptionValue}`)



let URLnoPagination =(`http://localhost:3000/data/products.json/products?&q=${filterText}&price_lte=${sliderValue}${includeSubscriptionValue}`)   

	fetch(URLnoPagination)
      .then(
      	(res) => res.json() //res.json()
      	)
      .then(
        (result) => {
       setAllItemsFound(result.length);
        },
        (error) => {
        //  setIsLoaded(true);
          setError(error);
        }
      )

    fetch(URL)
      .then(
      	(res) => res.json() //res.json()
      	)
      .then(
        (result) => {
       console.log(result);
          setIsLoaded(true);
          setItems(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [pageNumber, filterText, sliderValue,subscriptionValue])

 useEffect(() => {
		setPageNumber(1);
 },[filterText, sliderValue,subscriptionValue])



 if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } 
  else {
    return (
      
   		 	<>
  	<header className="App-header">
  		
        <img src={logo} className="App-logo" alt="logo" />
        <div>Product Collection Page</div>
  	</header>
    <div className="App-body">
	    <div className="App-body-filter-panel">
	      <SearchBar 
	        filterText={filterText} 
	        onFilterTextChange={setFilterText} 
	        maxValue={maxValue} 
	        sliderValue={sliderValue} 
	        onSliderValueChange={setSliderValue} 
  			subscriptionValue={subscriptionValue}
  			onSubscriptionValueChange={changeSubscriptionValue} />

      	</div>
      	<div>
      	{items.length !==0 ? 
      		<>
      	<div className="App-body-table-panel">
	      <ProductTable 
	        products={items} 
	        filterText={filterText} />
	     </div>
	        <div className="App-pagination">
	        	<button disabled={pageNumber===1} onClick={() =>setPageNumber(1)}>&#8676;</button>
	        	<button disabled={pageNumber===1} onClick={() =>setPageNumber(pageNumber-1)}>&#10229;</button>
		       	<button disabled={ items.length < itemsOnPage || allItemsFound === itemsOnPage }  onClick={() =>setPageNumber(pageNumber+1)}>&#10230;</button>
		       	<button disabled={ items.length < itemsOnPage || allItemsFound === itemsOnPage}  onClick={() =>goLast()}>&#8677;</button>
		       	{/*next button should be disabled when there is no second page - this requires 2 calls to the server!*/}
		       	 Page {pageNumber} of {1 + (allItemsFound === itemsOnPage?0:parseInt (allItemsFound / itemsOnPage))}
	        </div>
	        </>
	        :
	        <div className="no-matches">No matches to your search</div>

	       }
	     </div>
    </div>
    </>
    );
  }




}

