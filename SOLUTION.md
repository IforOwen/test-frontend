SOLUTION
========

Estimation
----------
Estimated: 4 hours

Spent: 6 hours
- Set up time was greater than expected
- Could not meet the Product Requirements with the data provided


Solution
--------
Solution works for small amount of data.
When searching for text - all fields are searched not just the tags
Could not get the pagination work fully via json-server so made 2 calls to the server and handled "first" and "last" via front end. 
I would not do this normally but given the time constraints I felt it was a necessary evil.

Components could be broken down further to support more reusability

Should use.scss rather than .css to streamline the styling

App is responsive, but UI could be improved.

More information could be displayed in the table

Imrpovements would take 16-24 hrs


TEST CASES

1. Feature:User moves from page to page in the table
 
Scenario: Table has more than one page
    Given There are 11 items in total
      And 8 items are displayed per page
     When the user clicks next
     Then the second page should be displayed with 3 items
      And the BACK button is disabled
      And the MOVE TO END button is disabled
      And the BACK button is enabled
      And the MOVE TO BEGINNING button is enabled
      And "Page 2 of 2" is displayed below the table




2. Scenario: Table has more than one page
    Given There are 11 items in total
      And 8 items are displayed per page
    When The user enters "chews" into the search field
     Then one page should be displayed with 8 items
      And the BACK button is disabled
      And the MOVE TO END button is disabled
      And the BACK button is disabled
      And the MOVE TO BEGINNING button is disabled
      And "Page 1 of 1" is displayed below the table



3. Scenario: Table has more than one page
    Given There are 11 items in total
      And 8 items are displayed per page
     When the user move the "Include subscriptions" slider to "YES"
     And enters "cat" in the search field
     Then one page should be displayed with 4 items


4. Scenario: Table has more than one page
    Given There are 11 items in total
      And 8 items are displayed per page
     When the user move the "price" slider to 25
     Then "No matches to yor search" is displayed instead of the table